<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoriesTest extends WebTestCase
{
    private function login()
    {
        $client = static::createClient();

        $client->request('POST', '/api/login', [], [], array('CONTENT_TYPE' => 'application/json'),
        json_encode(array(
          'email' => 'test@mail.com',
          'password' => 'test',
          ))
        );
        $data = json_decode($client->getResponse()->getContent(), true);
     
        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }

    public function testGetCategories()
    {
        $client = $this->login();

        $client->request('GET', '/api/categories');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetOneCategory()
    {
        $client = $this->login();

        $client->request('GET', '/api/categories/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
