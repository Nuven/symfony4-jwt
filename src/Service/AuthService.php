<?php 

namespace App\Service;

use App\Entity\User;
use Firebase\JWT\JWT;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use App\Exception\InvalidTokenException;
use Firebase\JWT\ExpiredException;

class AuthService implements JWTServiceInterface
{
    private $privateKey;

    private $publicKey;

    private $passPharse;

    private $tokenTtl;

    private $algorithm;

    public function __construct($privateKey, $publicKey, $passPharse, $tokenTtl, $algorithm)
    {
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
        $this->passPharse = $passPharse;
        $this->tokenTtl = $tokenTtl;
        $this->algorithm = $algorithm;
    }

    public function auth(User $user)
    {
        return $this->create($user);
    }

    public function create(UserInterface $user)
    {
        $privateKey = openssl_pkey_get_private('file:///' . $this->privateKey, $this->passPharse);
    
        $issuedAt = time();
        $iss = '';
        $exp = $issuedAt + $this->tokenTtl; 

        $payload = [
            //'iss' => 'test',
            'sub' => [$user->getEmail(),
                      'test'],
            'iat' => $issuedAt,
            'exp' => $exp,
        ];

        $jwt = JWT::encode($payload, $privateKey, 'RS256');

        return $jwt;
    }

    public function decode(TokenInterface $token) 
    {   
        try {
            $decoded = JWT::decode($token->getCredentials(), 'file:///' . $this->publicKey, [$this->algorithm]);
        } catch(\Exception $e) {
            throw new Exception($e->getMessage(), 401, $e);
        }
        return $decoded;
    }
}