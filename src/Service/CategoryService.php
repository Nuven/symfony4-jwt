<?php 

namespace App\Service;

use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryService 
{
    private $em;

    private $validator;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    public function createCategory($data)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($data);
        
        dd($form);
        //TODO: Add subcategory

        // if (empty($email) || empty($password)) {
        //     throw new BadCredentialsException("email and password cannot be null or empty", 400);
        // }

        // $user->setEmail($email);
        // $user->setPassword($this->encoder->encodePassword($user, $password));

        // if (count($errors = $this->validator->validate($user)) !== 0) {
        //     throw new ValidationException($errors, '400');
        // }

        // $this->em->persist($user);
        // $this->em->flush();

        // return $user;
    }
}