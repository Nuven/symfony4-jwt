<?php 

namespace App\Service;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

interface JWTServiceInterface
{
    public function create(UserInterface $user);

    public function decode(TokenInterface $token);
}