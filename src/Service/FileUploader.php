<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploader 
{
    private $saveTargetDirectory;

    private $targetDirectory;

    public function __construct($saveTargetDirectory, $targetDirectory)
    {
        $this->saveTargetDirectory = $saveTargetDirectory;
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file)
    {
        try {
            $file->move($this->getTargetDirectory(), $filename = $this->generateUniqueFileName($file));
        } catch(FileException $e) {
            throw new FileException($e->getMessage(), $e->getCode());
        }

        return [
            'filename' => $filename, 
            'path'     => $this->getPath($filename)
        ];
    }

    public function getTargetDirectory()
    {
        return $this->saveTargetDirectory;
    }

    public function getPath($filename) 
    {
        return $this->targetDirectory . '/' . $filename;
    }

    public function generateUniqueFileName(UploadedFile $file) 
    {
        return md5(uniqid()) . '.' . $file->guessExtension();
    }
}