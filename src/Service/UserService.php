<?php

namespace App\Service;

use App\Entity\User;
use App\Form\UserType;
use App\Exception\ValidationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private $em;

    private $validator;

    private $encoder;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->encoder = $encoder;
    }

    public function createUser($data)
    {
        // $user = new User();

        // $email = $data['email'];
        // $password = $data['password'];

        // if (empty($email) || empty($password)) {
        //     throw new BadCredentialsException("email and password cannot be null or empty", 400);
        // }

        // $user->setEmail($email);
        // $user->setPassword($this->encoder->encodePassword($user, $password));

        // if (count($errors = $this->validator->validate($user)) !== 0) {
        //     throw new ValidationException($errors, '400');
        // }

        // $this->em->persist($user);
        // $this->em->flush();

        // return $user;

        $form = $this->createForm(UserType::class, new Category(), ['csrf_protection'=>false]);
        $form->submit($data);
        dd($form);
        if($form->isSubmitted() && $form->isValid())
        {
            $user = $form->getData();
            $this->em->persist($user);
            $this->em->flush();
            return $user;
            //return new JsonResponse($this->serializer->serialize($category, 'json'), 200, [], true);
        }
        
        throw new InvalidArgumentException("Invalid ", 400);
    }

    public function getUser($data)
    {

        // $email = $data->get('email');
        // $password = $data->get('password');
        $email = $data['email'];
        $password = $data['password'];

        if (empty($email) || empty($password)) {
            throw new BadCredentialsException("email and password cannot be null or empty", 400);
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        if ($user) {
            if (!password_verify($password, $user->getPassword())) {
                throw new BadCredentialsException("Incorrect password", 400);
            }
            return $user;
        }


        throw new BadCredentialsException("Incorrect password or email", 400);
    }

    public function getUserByIdentifyField($identyfyField)
    {
        if (empty($identyfyField)) {
            throw new BadCredentialsException("identifyField cannot be null or empty", 400);
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $identyfyField]);

        if (!$user) {
            return false;
        }

        return $user;
    }
}
