<?php 

namespace App\TokenExtractor;

use Symfony\Component\HttpFoundation\Request;

class AuthorizationTokenExtractor 
{
    public function extract(Request $request)
    {
        if(!$request->headers->has('Authorization')) {
            return false;
        }

        $header = \explode(' ', $request->headers->get('Authorization'));
        
        if(count($header) !== 2 && 0 === strcasecmp($header[0], 'Bearer')) {
            return false;
        }
        
        return $header[1];
    }
}