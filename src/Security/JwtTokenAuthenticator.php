<?php

namespace App\Security;

use App\Service\AuthService;
use App\Security\JWTUserToken;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\TokenExtractor\AuthorizationTokenExtractor;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use App\Exception\InvalidTokenException;
use Symfony\Component\Config\Definition\Exception\Exception;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;

class JwtTokenAuthenticator extends AbstractGuardAuthenticator
{
    private $em;

    private $auth;

    private $tokenExctractor;

    private $userService;

    public function __construct(EntityManagerInterface $em, AuthService $auth, AuthorizationTokenExtractor $tokenExctractor, UserService $userService)
    {
        $this->em = $em;
        $this->auth = $auth;
        $this->tokenExctractor = $tokenExctractor;
        $this->userService = $userService;
    }

    public function supports(Request $request)
    {
        return $request->headers->has('Authorization');
    }

    public function getCredentials(Request $request)
    {
        if(($jwtToken = $this->tokenExctractor->extract($request)) === false) {
            throw new InvalidTokenException("Invalid token", 401);
        }
    
        $preAuthToken = new JWTUserToken($jwtToken);

        try {
            $decoded = $this->auth->decode($preAuthToken);
            
            // if(!$decoded = $this->auth->decode($preAuthToken)) {
            //     throw new InvalidTokenException('Invalid JWT Token');
            // }
        } catch (\Exception $e) {
            throw new InvalidTokenException($e->getMessage(), $e->getCode());
        }
        
        try {
            $preAuthToken->setPayload($decoded);
        } catch( Exception $e) {
            throw new InvalidTokenException($e->getMessage(), 401, $e);
        }
        
        return $preAuthToken;

    }

    public function getUser($preAuthToken, UserProviderInterface $userProvider)
    {
        $payload = $preAuthToken->getPayload();
        $userEmail = $payload->sub;
    
        $user = $this->userService->getUserByIdentifyField($userEmail);
     
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessage(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, $exception->getCode());
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, 401);
    }

    public function supportsRememberMe()
    {
        return false;
    }

}
