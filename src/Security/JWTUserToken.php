<?php 

namespace App\Security;

use Symfony\Component\Security\Guard\Token\PreAuthenticationGuardToken;

class JWTUserToken extends PreAuthenticationGuardToken
{
    private $token;

    private $payload; 

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function getCredentials()
    {
        return $this->token;
    }
    
    public function setPayload($payload)
    {
        $this->payload = $payload;
    }

    public function getPayload()
    {
        return $this->payload;
    }

}