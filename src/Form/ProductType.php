<?php

namespace App\Form;

use App\Entity\Image;
use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('categories', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'title',
                'multiple' => true
            ])
            ->add('images', FileType::class, [
                'label' => 'Upload images',
                'multiple' => true
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Create'
            ])
        ;

        $builder
            ->get('images')->addModelTransformer(new CallbackTransformer(
                function($image){
                    return;
                },
                function($files){
                    $images = [];
                    foreach($files as $file) {
                        $image = new Image();
                        $image->setName($file->getClientOriginalName());
                        $image->setFile($file);

                        $images[] = $image;
                    }

                    return $images;
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'csrf_protection' => false
        ]);
    }
}
