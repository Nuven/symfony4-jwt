<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use EXSyst\Component\Swagger\Response;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $response = new JsonResponse();

        $response->setContent(json_encode($this->getErrorMessage($exception)));
        //$response->setStatusCode($exception->getStatusCode());
        $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
           'kernel.exception' => 'onKernelException',
        ];
    }

    public function getErrorMessage(\Throwable $exception)
    {
        $error = [
            'message' => $exception->getMessage(),
        ];

        return $error;
    }
}
