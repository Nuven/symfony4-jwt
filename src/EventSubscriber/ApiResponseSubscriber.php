<?php

namespace App\EventSubscriber;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ApiResponseSubscriber implements EventSubscriberInterface
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        //TODO: Make some filter for json response
        
        // $message = $event->getResponse()->getContent();
        // dd($message);
        // $statusCode = $event->getResponse()->getStatusCode();

        // $response = new JsonResponse(['message' => $message], 401);

       
        // $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
           'kernel.response' => 'onKernelResponse',
        ];
    }
}
