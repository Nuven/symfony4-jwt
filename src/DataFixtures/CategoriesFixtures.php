<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CategoriesFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category = new Category();

        $category->setName('Test2 category');
        $category->addProduct($this->getReference('product1'));
        $manager->persist($category);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ProductsFixtures::class,
        );
    }
}
