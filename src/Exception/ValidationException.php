<?php 

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\HttpKernel\Exception\HttpException;


class ValidationException extends HttpException
{
    private $violations;

    public function __construct(ConstraintViolationList $violations, int $code)
    {
        $this->violations = $violations;
        parent::__construct(400, $this->getMessages(), null, [], $code);
    }

    public function getMessages() 
    {
        $messages = [];

        
        foreach($this->violations as $violation) {
            $messages[] = $violation->getMessage();
        }

        //return json_encode($messages);
        return implode(', ', $messages);
    }

}