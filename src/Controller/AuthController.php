<?php

namespace App\Controller;

use App\Service\AuthService;
use App\Service\UserService;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthController extends AbstractController
{
    /**
     * @Route(
     * 		path="/api/login", 
     * 		methods={"POST"},
     * 		name="api_login"
     * );
     * 
     * @SWG\Parameter(
     * 		name="body",
     * 		in="body",
     * 		description="Authentication",
     * 		required=true,
     * 		@SWG\Schema(
     * 			example={"email":"email","password":"password"}
     * 		)
     * )
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Successfully authenticated",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad credentials"
     * )
     */
    public function login(Request $request, UserService $userService, AuthService $authService)
    {
        $data = (array)json_decode($request->getContent());

        // dd($data = json_decode($jsonData)->email);
        $user = $userService->getUser($data);

        $jwt = $authService->auth($user);

        return new JsonResponse(['token' => $jwt], 200);
    }
}
