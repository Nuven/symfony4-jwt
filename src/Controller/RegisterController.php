<?php

namespace App\Controller;

use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\UserService;

class RegisterController extends AbstractController
{
    /**
     * @Route(
     * 		path="/api/register", 
     * 		methods={"POST"},
     * 		name="api_register"
     * );
     * 
     * @SWG\Parameter(
     * 		name="body",
     * 		in="body",
     * 		description="Registration",
     * 		required=true,
     * 		@SWG\Schema(
     * 			example={"email":"email","password":"password"}
     * 		)
     * )
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Successfully registered",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad credentials"
     * )
     */
    public function register(Request $request, UserService $userService, SerializerInterface $serializer)
    {
        $data = (array)json_decode($request->getContent());

        $user = $userService->createUser($data);

        $jsonContent = $serializer->serialize($user, 'json', ['groups' => 'User']);

        return new JsonResponse($jsonContent, 201, [], true);
    }
}
