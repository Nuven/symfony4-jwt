<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use Swagger\Annotations as SWG;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Exception\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CategoryController extends AbstractController
{
    private $em;

    private $validator;

    private $serializer;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->serializer = $serializer;    
    } 
    /**
     * @Route(
     * 		path="/api/categories", 
     * 		methods={"GET"},
     * 		name="api_get_categories"
     * );
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="List of categories",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function getCategories()
    {
        $categories = $this->em->getRepository(Category::class)->findAll();

        return new JsonResponse($this->serializer->serialize($categories, 'json'), 200, [], true);
    }

    /**
     * @Route(
     * 		path="/api/categories", 
     * 		methods={"POST"},
     * 		name="api_post_categories"
     * );
     * 
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Post a category",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function postCategories(Request $request) 
    {        
        $category = $this->createAndSubmitForm($request);
        return new JsonResponse($this->serializer->serialize($category, 'json'), 200, [], true);
    }

    /**
     * @Route(
     * 		path="/api/categories/{id}", 
     * 		methods={"PATCH"},
     * 		name="api_patch_category"
     * );
     * 
     * @ParamConverter("category", class="App\Entity\Category")
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Update the category",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function patchCategory(Request $request, Category $category = null)
    {
        if(!$category) {
            throw new NotFoundHttpException('Category not found', null, 404);
        }
        
        $data = json_decode($request->getContent(), true);

        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($data, false);
        
        if($form->isSubmitted() && $form->isValid())
        {
            $this->em->persist($category);
            $this->em->flush();
            return new JsonResponse($this->serializer->serialize($category, 'json'), 200, [], true);
        }

        throw new BadRequestHttpException("Bad request", null, 400);

    }

    /**
     * @Route(
     * 		path="/api/categories/{id}", 
     * 		methods={"DELETE"},
     * 		name="api_delete_category"
     * );
     * 
     * @ParamConverter("category", class="App\Entity\Category")
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Delete the category",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function deleteCategory(Category $category = null) 
    {
        if(!$category) {
            throw new NotFoundHttpException('Category not found', null, 404);
        }
        try {
            $this->em->remove($category);
            $this->em->flush();
        } catch(\PDOException $e) {
            dd($e);
            //throw new ($e->getMessage(), $e->getCode(), $e);
        }
        
        return new JsonResponse(['message' => 'Category successfully deleted'], 200);
    }

    /**
     * @Route(
     * 		path="/api/categories/{id}", 
     * 		methods={"GET"},
     * 		name="api_get_category"
     * );
     * 
     * @ParamConverter("category", class="App\Entity\Category")
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Returns a category",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function getCategory(Category $category = null)
    {
        if(!$category) {
            throw new NotFoundHttpException('Category not found', null, 404);
        }
        return new JsonResponse($this->serializer->serialize($category, 'json'), 200, [], true);
    }

    private function createAndSubmitForm(Request $request, Category $category = null)
    {
        $data = json_decode($request->getContent(), true);
        $category = $category ?? new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($data);
        if($form->isSubmitted() && $form->isValid())
        {
            $category = $form->getData();
            if (count($errors = $this->validator->validate($category)) !== 0) {
                throw new ValidationException($errors, 400);
            }
            $this->em->persist($category);
            $this->em->flush();
            return $category;
        }
        
        throw new BadRequestHttpException($form->getErrors(true)->getChildren()->getMessage(), null, 400);
    }

}
