<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Product;
use App\Form\ProductType;
use App\Service\FileUploader;
use Swagger\Annotations as SWG;
use App\Exception\ValidationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ProductController extends AbstractController
{
    private $em;

    private $validator;

    private $serializer;

    private $uploader;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator, SerializerInterface $serializer, FileUploader $uploader)
    {
        $this->em = $em;
        $this->validator = $validator;    
        $this->serializer = $serializer;
        $this->uploader = $uploader;

    }
    /**
     * @Route(
     * 		path="/api/products", 
     * 		methods={"GET"},
     * 		name="api_get_products"
     * );
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Successfully",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function getProducts() 
    {
        $products = $this->em->getRepository(Product::class)->findAll();

        return new JsonResponse($this->serializer->serialize($products, 'json'), 200, [], true);
    }

     /**
     * @Route(
     * 		path="/products", 
     * 		methods={"POST"},
     * 		name="api_post_products"
     * );
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Successfully",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function postProducts(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        dd($data);
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $images = $product->getImages();
            $product = $form->getData();
            
            foreach($images as $image) {
                $uploadData = $this->uploader->upload($image->getFile());

                $image->setOriginalPath($uploadData['path']);
                $image->setPath($uploadData['path']);
                
                $this->em->persist($image);
            }
            $this->em->persist($product);
            $this->em->flush();
        }

        // return $this->render('product/index.html.twig', [
        //     'form' => $form->createView(),
        // ]);
        

       // $product = $this->createAndSubmitForm($request);
        return new JsonResponse($this->serializer->serialize($product, 'json'), 200, [], true);
    }

     /**
     * @Route(
     * 		path="/products/{id}/update", 
     * 		methods={"GET"},
     * 		name="api_patch_products"
     * );
     * 
     * @SWG\Response(
     * 		response=200,
     * 		description="Successfully",
     * )
     * 
     * @SWG\Response(
     * 		response=400,
     * 		description="Bad request"
     * )
     */
    public function patchProducts(Request $request, Product $product = null)
    {

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {

            $images = $product->getImages();
            $product = $form->getData();
         
            foreach($images as $image) {
                $uploadData = $this->uploader->upload($image->getFile());

                $image->setOriginalPath($uploadData['path']);
                $image->setPath($uploadData['path']);
                
                $this->em->persist($image);
            }
            $this->em->persist($product);
            $this->em->flush();
        }

        return $this->render('product/index.html.twig', [
            'form' => $form->createView(),
            'images' => $product->getImages(),
            'categories' => $product->getCategories()
        ]);
    }

    private function createAndSubmitForm(Request $request, Product $product = null)
    {
        $data = json_decode($request->getContent(), true);
        $product = $product ?? new Product();

        $form = $this->createForm(Product::class, $product);
        $form->submit($data);
        if($form->isSubmitted() && $form->isValid())
        {
            $product = $form->getData();
            dd($product);
            if (count($errors = $this->validator->validate($product)) !== 0) {
                throw new ValidationException($errors, 400);
            }
            $this->em->persist($product);
            $this->em->flush();
            return $product;
        }
        
        throw new BadRequestHttpException($form->getErrors(true)->getChildren()->getMessage(), null, 400);
    }
}
